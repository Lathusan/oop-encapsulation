package com.oopBasic.encapsulation;

import java.util.Scanner;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0 Jan 28, 2021
 **/

public class EncapsulationDemo {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);

		Encapsulation s = new Encapsulation();
		
		System.out.print("Enter your name : ");
		s.setName(scan.next());
		
		System.out.print("Enter your age  : ");
		s.setAge(scan.nextInt());
		
		System.out.print("Enter your city : ");
		s.setCity(scan.next());
		
		System.out.println("I'm " + s.getName() + " " + s.getAge() + " years old. " + "I lived in " + s.getCity() + ", Sri Lanka.");
	}

}
